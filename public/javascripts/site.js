var helpers = {
  doXHR: function(options) {
    return new Promise(function(resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.open(options.method, options.url);

      xhr.timeout = 10000;

      xhr.onload = function() {
        if (xhr.status >= 200 && xhr.status < 300) {
          return resolve(xhr);
        }

        return reject(xhr);
      };

      xhr.ontimeout = function(e) {
        return reject(e);
      };

      xhr.onerror = function(e) {
        return reject(e);
      };

      if (options.headers) {
        Object.keys(options.headers).forEach(function(k) {
          xhr.setRequestHeader(k, options.headers[k]);
        });
      }

      var data = options.data;
      xhr.send(data);
    });
  },
  closestSibling: function(elem, className) {
    if (!elem || elem === document) {
      return null;
    }

    var sibling = elem.parentNode.firstChild;
    for (; sibling; sibling = sibling.nextSibling) {
      if (sibling.nodeType === 1 && sibling !== elem && sibling.classList.contains(className)) {
        return sibling;
      }
    }

    return null;
  },
  emptyChildren: function(node) {
    while(node.firstChild) {
      node.removeChild(node.firstChild);
    }
  },
};

function getCheckoutToken(phoneNumber) {
  var queryString = 'phoneNumber=' + encodeURIComponent(phoneNumber);

  return helpers.doXHR({
    method: 'POST',
    url: 'https://api.africastalking.com/checkout/token/create',
    data: queryString,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .then(function(result) {
    var data = JSON.parse(result.response);
    if (data.description !== 'Success') throw new Error(data.description);
    return data.token;
  });
};

document.addEventListener('DOMContentLoaded', function() {
  try {
    var modal = new VanillaModal({
      open: '.open-modal',
      close: '.close-modal',
      clickOutside: false
    });
  } catch (e) {}

  (function mountSubscribeForm() {
    var subscribeForm = document.querySelector('.js-subscribe-form');
    var submitBtn = subscribeForm.querySelector('.js-submit-form');
    var responseWrapper = subscribeForm.querySelector('.js-response-wrapper');
    var errorWrapper = subscribeForm.querySelector('.js-error-wrapper');
    subscribeForm.addEventListener('submit', subscribeHandler);

    function subscribeHandler(evt) {
      evt.preventDefault();

      var subscribeParams = {
        phoneNumber: subscribeForm.querySelector('.js-phone-number').value,
        agreed: subscribeForm.querySelector('.js-terms').checked
      };

      if (!validate(subscribeParams)) return;

      submitBtn.innerText = 'Sending subscription request...';
      submitBtn.setAttribute('disabled', 'disabled');
      helpers.emptyChildren(responseWrapper);

      getCheckoutToken(subscribeParams.phoneNumber)
      .then(function(token) {
        subscribeParams.checkoutToken = token;

        return submitSubscribe(subscribeParams)
        .then(function(result) {
          console.log(result);

          var span = document.createElement('span');
          if (result.created) {
            span.innerText = 'Reply with \'1\' to the subscription pop up on your phone';
            responseWrapper.appendChild(span);
          } else {
            span.classList.add('error');
            span.innerText = 'Subscription failed, retry after a few minutes';
            responseWrapper.appendChild(span);
          }

          submitBtn.innerText = 'Subscribe';
          submitBtn.removeAttribute('disabled');
        });
      })
      .catch(function(err) {
        console.log(err);

        var span = document.createElement('span');
        span.classList.add('error');
        span.innerText = 'Subscription failed, retry after a few minutes';
        responseWrapper.appendChild(span);

        submitBtn.innerText = 'Subscribe';
        submitBtn.removeAttribute('disabled');
      });
    }

    function submitSubscribe(subscribeParams) {
      var postData = JSON.stringify(subscribeParams);
      return helpers.doXHR({
        method: subscribeForm.getAttribute('method'),
        url: subscribeForm.getAttribute('action'),
        data: postData,
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then(function(result) {
        var data = JSON.parse(result.response);
        return data;
      });
    }

    function validate(params) {
      helpers.emptyChildren(errorWrapper);
      var errors = [];

      if (!params.agreed) errors.push('You must agree to the terms');
      if (params.phoneNumber.length !== 10) errors.push('Phone Number is required and must be valid');

      if (errors.length > 0) {
        errors.forEach(function(err) {
          var span = document.createElement('span');
          span.innerText = err;
          errorWrapper.appendChild(span);
        });

        return false;
      }

      return true;
    }
  })();
});
