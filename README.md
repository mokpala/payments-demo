### SMS Subscription Sample App

Sample app testing the new intermediary API for create sms subscription

### How to run

Install node.js version ver >6.x.x

Create a `.env` file in the projects root directory with the following:

```
AT_USERNAME=
AT_APIKEY=
```

Install Node Modules and start the app

```bash
$ npm install
$ npm start
```

### Routes

- Routes are under routes folder (index, subscribe)
- they are mapped by app.js (look at the route imports)
